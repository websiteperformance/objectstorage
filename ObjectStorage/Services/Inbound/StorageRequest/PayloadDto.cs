using System.Text.Json.Serialization;

namespace ObjectStorage.Services.Inbound.StorageRequest;

public enum PayloadTypeEnum
{
    Timing,
    PerformanceEntry,
    Screenshot, 
    Tracing,
    Networks
}


public class PayloadDto
{
    [JsonPropertyName("id")] public Guid Id { get; set; }
    [JsonPropertyName("payloadType")] public PayloadTypeEnum PayloadTypeEnum { get; set; }
    
    [JsonPropertyName("data")] public byte[]? Data { get; set; }
    
    [JsonPropertyName("contentType")] public string? ContentType { get; set; } 

}