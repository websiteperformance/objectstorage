using Silverback.Messaging.Messages;

namespace ObjectStorage.Services.Inbound.StorageRequest;

public class StorageRequestSubscriber(ILogger<StorageRequestSubscriber> logger, IStorageProcessor storageProcessor)
{
    public Task OnWrappedMessageReceived(IInboundEnvelope<PayloadDto> envelope)
    {
        if (envelope.Message != null)
        {
            logger.LogInformation("received a message, lets process as envelope, possible chunked");
            var asyncArray = new[] {envelope.Message};
            var asyncMapper = asyncArray.ToAsyncEnumerable();
            return storageProcessor.ProcessAsync(asyncMapper);
        }

        logger.LogWarning("cannot process enveloped message, payload is null");

        return Task.CompletedTask;
    }

}