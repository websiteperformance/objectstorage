using Confluent.Kafka;
using Silverback.Messaging.Configuration;

namespace ObjectStorage.Services.Inbound.StorageRequest;

public class StorageRequestsEndpointConfigurator(IConfigurationHandler configurationHandler)
    : IEndpointsConfigurator
{
    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints =>
                endpoints.Configure(config =>
                    {
                        config.BootstrapServers = configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                    })
                    .AddInbound(endpoint =>
                        endpoint
                            .ConsumeFrom(configurationHandler.GetValue<string>("KAFKA_JOBSTORAGE_REQUESTS")).Configure(
                                config =>
                                {
                                    config.GroupId =
                                        configurationHandler.GetValue<string>("KAFKA_JOBSTORAGE_GROUPID");
                                    config.AutoOffsetReset = AutoOffsetReset.Latest;
                                })
                            .DeserializeJson(ser => ser.UseFixedType<PayloadDto>()))
            );
    }
}