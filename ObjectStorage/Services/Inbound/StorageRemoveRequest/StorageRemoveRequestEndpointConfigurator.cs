using Confluent.Kafka;
using ObjectStorage.Services.Inbound.StorageRequest;
using Silverback.Messaging.Configuration;

namespace ObjectStorage.Services.Inbound.StorageRemoveRequest;

public class StorageRemoveRequestEndpointConfigurator
    (IConfigurationHandler configurationHandler) : IEndpointsConfigurator
{
    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints =>
                endpoints.Configure(config =>
                    {
                        config.BootstrapServers = configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                    })
                    .AddInbound(endpoint =>
                        endpoint
                            .ConsumeFrom(configurationHandler.GetValue<string>("KAFKA_REMOVESTORAGE_INBOUND")).Configure(
                                config =>
                                {
                                    config.GroupId =
                                        configurationHandler.GetValue<string>("KAFKA_JOBSTORAGE_GROUPID");
                                    config.AutoOffsetReset = AutoOffsetReset.Latest;
                                })
                            .DeserializeJson(ser => ser.UseFixedType<PayloadDto>()))
            );
        
    }
}