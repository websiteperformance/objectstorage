using ObjectStorage.Services.Inbound.StorageRequest;

namespace ObjectStorage.Services.Inbound.StorageRemoveRequest;

public class StorageRemoveSubscriber(
    IRemoveStorageProcessor removeStorageProcessor)
{
    public Task OnRemoveMessageReceived(IAsyncEnumerable<PayloadDto> batch)
    {
        return removeStorageProcessor.ProcessAsync(batch);
    }
}