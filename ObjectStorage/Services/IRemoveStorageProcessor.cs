using ObjectStorage.Services.Inbound.StorageRemoveRequest;
using ObjectStorage.Services.Inbound.StorageRequest;

namespace ObjectStorage.Services;

public interface IRemoveStorageProcessor
{
    public Task ProcessAsync(IAsyncEnumerable<PayloadDto> storageRemoveMessages);
}