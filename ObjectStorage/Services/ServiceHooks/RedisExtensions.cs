﻿using StackExchange.Redis;

namespace ObjectStorage.Services.ServiceHooks;

public static class RedisExtensions
{
    
    public static void AddRedis(this IServiceCollection serviceCollection, IConfigurationHandler configurationHandler)
    {
        var options = new ConfigurationOptions
        {
            EndPoints =
            {
                {
                    configurationHandler.GetValue<string>("REDIS_ENDPOINT_ADDRESS"),
                    configurationHandler.GetValue<int>("REDIS_ENDPOINT_PORT")
                }
            }
        };

        var multiplexer = ConnectionMultiplexer.Connect(options);
        serviceCollection.AddSingleton<IConnectionMultiplexer>(multiplexer);
    }
}