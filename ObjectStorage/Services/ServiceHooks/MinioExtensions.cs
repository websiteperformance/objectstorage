using Minio;

namespace ObjectStorage.Services.ServiceHooks;

public static class MinioExtensions
{
    public static void AddMinio(this IServiceCollection serviceCollection, IConfigurationHandler configurationHandler)
    {

        IMinioClient minioClient = new MinioClient()
            .WithEndpoint(configurationHandler.GetValue<string>("MINIO_URL"))
            .WithCredentials(configurationHandler.GetValue<string>("MINIO_ACCESS_KEY"), configurationHandler.GetValue<string>("MINIO_SECRET_KEY"))
            .WithSSL(false)
            .Build();
        serviceCollection.AddSingleton<IMinioClient>(minioClient);
    }
}