using Akka;
using Akka.Actor;
using Akka.Streams;
using Akka.Streams.Dsl;
using Akka.Streams.Implementation.Fusing;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Outbound.StorageResponse;
using ObjectStorage.Services.Storage;

namespace ObjectStorage.Services;

public class StorageProcessor(ILogger<StorageProcessor> logger, ActorSystem actorSystem, IStorageService storageService,
        IStoreResponseMessageProducer storeResponseMessageProducer)
    : IStorageProcessor
{
    private readonly ActorMaterializer _actorMaterializer = ActorMaterializerExtensions.Materializer(actorSystem);

    public async Task ProcessAsync(IAsyncEnumerable<PayloadDto> storageMessages)
    {
        await Source.FromGraph(new AsyncEnumerable<PayloadDto>(() => storageMessages))
            .WhereNot(dto =>
            {
                var res = dto.ContentType is null || dto.Data is null;
                if (!res) return res;
                logger.LogWarning("Skip message, either contentType or data should not be null for element {S}",
                    dto.Id.ToString());
                return res;
            })
            .SelectAsync(1, async data => await storageService.DoStorePayloadAsync(data, new CancellationToken()))
            .SelectAsync(1, async d => await storeResponseMessageProducer.PublishAsync(d))
            .RunWith(Sink.Ignore<bool>(), _actorMaterializer);
    }
}