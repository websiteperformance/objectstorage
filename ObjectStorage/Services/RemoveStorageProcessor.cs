using Akka.Actor;
using Akka.Streams;
using Akka.Streams.Dsl;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Storage;

namespace ObjectStorage.Services;

public class RemoveStorageProcessor(
    ActorSystem actorSystem,
    IStorageService storageService) : IRemoveStorageProcessor
{
    private readonly ActorMaterializer _actorMaterializer = ActorMaterializerExtensions.Materializer(actorSystem);

    public async Task ProcessAsync(IAsyncEnumerable<PayloadDto> storageRemoveMessages)
    {
        await Source.From(() => storageRemoveMessages)
            .SelectAsync(2, obj => storageService.DoDeleteObjectAsync(obj, new CancellationToken()))
            .RunWith(Sink.Ignore<bool>(), _actorMaterializer);
    }
}