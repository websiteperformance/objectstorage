﻿using LanguageExt;

namespace ObjectStorage.Services.Cache;

public interface IStorageCacheAsync
{

    Task<Option<byte[]>> GetItemFromCacheAsync(Guid id);

    Task SetItemToCacheAsync(Guid id, byte[] value);

    Task RemoveItemFromCacheAsync(Guid id);

}