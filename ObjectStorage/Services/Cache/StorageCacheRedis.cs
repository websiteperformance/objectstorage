﻿using LanguageExt;
using MethodTimer;
using StackExchange.Redis;

namespace ObjectStorage.Services.Cache;

public class StorageCacheRedis(ILogger<StorageCacheRedis> logger, IConnectionMultiplexer connectionMultiplexer)
    : IStorageCacheAsync
{
    private readonly ILogger<StorageCacheRedis> _logger = logger;
    private readonly IDatabase _database = connectionMultiplexer.GetDatabase();

    [Time]
    public async Task<Option<byte[]>> GetItemFromCacheAsync(Guid id)
    {
        var result = await _database.StringGetAsync(id.ToString());
        if (result.HasValue)
            return (byte[]) result!;

        return Option<byte[]>.None;
    }

    [Time]
    public async Task SetItemToCacheAsync(Guid id, byte[] value)
    {
        await _database.StringSetAsync(id.ToString(), value, null, When.NotExists, CommandFlags.None);
    }

    [Time]
    public async Task RemoveItemFromCacheAsync(Guid id)
    {
        await _database.KeyDeleteAsync(id.ToString());
    }
}