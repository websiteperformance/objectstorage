namespace ObjectStorage.Services.Outbound.StorageResponse;

public interface IStoreResponseMessageProducer
{
    public Task<bool> PublishAsync(PayloadResponse payloadResponse);
}