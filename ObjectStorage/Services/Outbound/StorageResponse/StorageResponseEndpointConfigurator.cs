using Silverback.Messaging.Configuration;

namespace ObjectStorage.Services.Outbound.StorageResponse;

public class StorageResponseEndpointConfigurator(ILogger<StorageResponseEndpointConfigurator> logger,
        IConfigurationHandler configurationHandler)
    : IEndpointsConfigurator
{
    private readonly ILogger<StorageResponseEndpointConfigurator> _logger = logger;


    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints => endpoints.Configure(config =>
                {
                    config.BootstrapServers = configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                })
                .AddOutbound<PayloadResponseDto>(endpoint => endpoint
                    .ProduceTo(configurationHandler.GetValue<string>("KAFKA_JOBSTORAGE_RESPONSES"))
                    .SerializeAsJson(ser => ser.UseFixedType<PayloadResponseDto>())));
    }
}