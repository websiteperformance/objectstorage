using Silverback.Messaging.Publishing;

namespace ObjectStorage.Services.Outbound.StorageResponse;

public class StoreResponseMessageProducer(ILogger<StoreResponseMessageProducer> logger, IPublisher publisher)
    : IStoreResponseMessageProducer
{
    public async Task<bool> PublishAsync(PayloadResponse payloadResponse)
    {
        try
        {
            logger.LogInformation("receive information to publish for {ResponseId}", payloadResponse.Id);
            PayloadResponseDto to = payloadResponse;
            await publisher.PublishAsync<PayloadResponseDto>(to);
            return true;
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "An error occured");
            return false;
        }
    }
}