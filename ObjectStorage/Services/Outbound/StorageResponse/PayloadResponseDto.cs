using System.Text.Json.Serialization;

namespace ObjectStorage.Services.Outbound.StorageResponse;

public class PayloadResponseDto
{
    [JsonPropertyName("id")] public Guid Id { get; set; }
    [JsonPropertyName("responseId")] public Guid? ResponseId { get; set; }
    
    [JsonPropertyName("response_message")] public string? ResponseMessage { get; set; }
    
    [JsonPropertyName("storage_type")] public string? StorageType { get; set; }

    public static implicit operator PayloadResponseDto(PayloadResponse payloadResponse) => new()
    {
        Id = payloadResponse.Id, ResponseId = payloadResponse.ResponseId,
        ResponseMessage = payloadResponse.ResponseMessage, StorageType = payloadResponse.BucketName?.ToString()
    };

}

public class PayloadResponse
{
    public Guid Id { get; set; }
    
    public Guid? ResponseId { get; set; }
    
    public string? ResponseMessage { get; set; }
    
    public BucketName? BucketName { get; set; }
}