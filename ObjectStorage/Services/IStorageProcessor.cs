using ObjectStorage.Services.Inbound.StorageRequest;

namespace ObjectStorage.Services;

public interface IStorageProcessor
{
    public Task ProcessAsync(IAsyncEnumerable<PayloadDto> storageMessages);
}