using LanguageExt;

namespace ObjectStorage.Services;

public class ConfigurationHandler: IConfigurationHandler
{
    private readonly IConfiguration _configuration;
    
    public ConfigurationHandler(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    
    public T GetValue<T>(string key)
    {
        return _configuration.GetValue<T>(key) ??
               throw new ValueIsNullException($"value for key {key} cannot be found in configuration");
    }
    
}