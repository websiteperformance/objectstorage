using LanguageExt;

namespace ObjectStorage.Services.Storage;

public interface IMinioStorage
{
    Task<bool> CheckIfBucketExistsAsync(BucketName bucketName);

    Task CreateBucketAsync(BucketName bucketName);

    Task UploadDataAsync(Stream dataStream, BucketName bucketName, string objectName, string contentType);

    Task<Option<MemoryStream>> DownloadObjectAsync(BucketName bucketName, string objectName);

    Task RemoveObjectFromBucketAsync(BucketName bucketName, string objectName);
}