using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Outbound.StorageResponse;

namespace ObjectStorage.Services.Storage;

public class StorageService(ILogger<StorageService> logger, IMinioStorage minioStorage) : IStorageService
{
    public async Task<bool> DoDeleteObjectAsync(PayloadDto payloadDto, CancellationToken cancellationToken)
    {
        try
        {
            var bucket = BucketName.GetBucketNameByEnumType(payloadDto.PayloadTypeEnum);
            await minioStorage.RemoveObjectFromBucketAsync(bucket, payloadDto.Id.ToString());
            logger.LogInformation("successfully remove object {ObjectId} from bucket {BucketName}", payloadDto.Id,
                bucket.ToString());
            return true;
        }
        catch (Exception e)
        {
            logger.LogError(e, "an error occured while removing object {ObjectId} from bucket {BucketName}",
                payloadDto.Id, payloadDto.PayloadTypeEnum);
            return false;
        }
    }

    public async Task<PayloadResponse> DoStorePayloadAsync(PayloadDto payloadDto, CancellationToken cancellationToken)
    {
        try
        {
            var bucket = BucketName.GetBucketNameByEnumType(payloadDto.PayloadTypeEnum);
            if (!await minioStorage.CheckIfBucketExistsAsync(bucket))
                await minioStorage.CreateBucketAsync(bucket);

            var gd = Guid.NewGuid();
            await minioStorage.UploadDataAsync(new MemoryStream(payloadDto.Data!), bucket, gd.ToString(),
                payloadDto.ContentType!);

            return new PayloadResponse
            {
                Id = payloadDto.Id,
                ResponseId = gd,
                ResponseMessage = "Ok",
                BucketName = bucket
            };
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "An error occured");
            return new PayloadResponse
            {
                Id = payloadDto.Id,
                ResponseMessage = exception.Message
            };
        }
    }
}