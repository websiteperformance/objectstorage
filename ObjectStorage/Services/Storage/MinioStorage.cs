using LanguageExt;
using Minio;
using Minio.DataModel.Args;
using static LanguageExt.Prelude;

namespace ObjectStorage.Services.Storage;

public class MinioStorage(ILoggerFactory loggerFactory, IMinioClientWrapper minioClientWrapper)
    : IMinioStorage
{
    private readonly ILogger<MinioStorage> _logger = LoggerFactoryExtensions.CreateLogger<MinioStorage>(loggerFactory);

    public Task RemoveObjectFromBucketAsync(BucketName bucketName, string objectName)
    {
        var roArgs = new RemoveObjectArgs()
            .WithBucket(bucketName.ToString())
            .WithObject(objectName);

        return minioClientWrapper.RemoveFromBucketAsync(roArgs, new CancellationToken());
    }

    public Task<bool> CheckIfBucketExistsAsync(BucketName bucketName)
    {
        var beArgs = new BucketExistsArgs()
            .WithBucket(bucketName.ToString());
        return minioClientWrapper.BucketExistsAsync(beArgs, new CancellationToken());
    }

    public async Task<Option<MemoryStream>> DownloadObjectAsync(BucketName bucketName, string objectName)
    {
        try
        {
            var memResult = new MemoryStream();

            var statArgs = new GetObjectArgs()
                .WithBucket(bucketName.ToString())
                .WithObject(objectName)
                .WithCallbackStream(stream => stream.CopyToAsync(memResult));

            await minioClientWrapper.GetObjectAsync(statArgs, new CancellationToken());

            return memResult;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "expected error while getting state of minio object");
            return None;
        }
    }

    public Task CreateBucketAsync(BucketName bucketName)
    {
        var mbArgs = new MakeBucketArgs()
            .WithBucket(bucketName.ToString());
        return minioClientWrapper.MakeBucketAsync(mbArgs, new CancellationToken());
    }

    public async Task UploadDataAsync(Stream dataStream, BucketName bucketName, string objectName, string contentType)
    {
        var putObjectArgs = new PutObjectArgs()
            .WithBucket(bucketName.ToString())
            .WithObject(objectName)
            .WithStreamData(dataStream)
            .WithObjectSize(dataStream.Length)
            .WithContentType(contentType);
        await minioClientWrapper.PutObjectAsync(putObjectArgs, new CancellationToken());
    }
}