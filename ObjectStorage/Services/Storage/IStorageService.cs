using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Outbound.StorageResponse;

namespace ObjectStorage.Services.Storage;

public interface IStorageService
{

    public Task<bool> DoDeleteObjectAsync(PayloadDto payloadDto, CancellationToken cancellationToken);
    public Task<PayloadResponse> DoStorePayloadAsync(PayloadDto payloadDto, CancellationToken cancellationToken);
}