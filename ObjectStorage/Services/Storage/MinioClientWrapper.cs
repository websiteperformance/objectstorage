using Minio;
using Minio.DataModel.Args;

namespace ObjectStorage.Services.Storage;

public class MinioClientWrapper(IMinioClient client) : IMinioClientWrapper
{
    public Task<bool> BucketExistsAsync(BucketExistsArgs args, CancellationToken cancellationToken)
    {
        
        return client.BucketExistsAsync(args, cancellationToken);
    }

    public Task GetObjectAsync(GetObjectArgs args, CancellationToken cancellationToken)
    {
        return client.GetObjectAsync(args, cancellationToken);
    }

    public Task MakeBucketAsync(MakeBucketArgs args, CancellationToken cancellationToken)
    {
        return client.MakeBucketAsync(args, cancellationToken);
    }

    public Task PutObjectAsync(PutObjectArgs args, CancellationToken cancellationToken)
    {
        return client.PutObjectAsync(args, cancellationToken);
    }

    public Task RemoveFromBucketAsync(RemoveObjectArgs args, CancellationToken cancellationToken)
    {
        return client.RemoveObjectAsync(args, cancellationToken);
    }
}