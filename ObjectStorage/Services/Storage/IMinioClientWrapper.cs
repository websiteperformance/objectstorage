using Minio;
using Minio.DataModel.Args;

namespace ObjectStorage.Services.Storage;

public interface IMinioClientWrapper
{
    Task<bool> BucketExistsAsync(BucketExistsArgs args, CancellationToken cancellationToken);
    Task GetObjectAsync(GetObjectArgs args, CancellationToken cancellationToken);

    Task MakeBucketAsync(MakeBucketArgs args, CancellationToken cancellationToken);

    Task PutObjectAsync(PutObjectArgs args, CancellationToken cancellationToken);

    Task RemoveFromBucketAsync(RemoveObjectArgs args, CancellationToken cancellationToken);
}