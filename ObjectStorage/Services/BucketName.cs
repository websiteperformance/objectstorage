using ObjectStorage.Services.Inbound.StorageRequest;

namespace ObjectStorage.Services;

public class BucketName
{
    private readonly string _bucketName;
    private readonly string _contentType;

    private BucketName(string bucketName, string contentType)
    {
        _bucketName = bucketName;
        _contentType = contentType;
    }

    public override string ToString() => _bucketName;
    public string ContentType() => _contentType;

    
    public static BucketName GetBucketNameByEnumType(PayloadTypeEnum payloadTypeEnum) =>
        payloadTypeEnum switch
        {
            PayloadTypeEnum.Screenshot => Screenshots,
            PayloadTypeEnum.Timing => Timings,
            PayloadTypeEnum.PerformanceEntry => PerformanceEntries,
            PayloadTypeEnum.Tracing => Tracings,
            PayloadTypeEnum.Networks => Networks,
            _ => throw new ArgumentOutOfRangeException(nameof(PayloadTypeEnum),
                $"invalid value for type received {payloadTypeEnum}")
        };
    
    
    public static readonly BucketName PerformanceEntries = new("performanceentries", "application/json");
    public static readonly BucketName Screenshots = new("screenshots", "image/png");
    public static readonly BucketName Timings = new("timings", "application/json");
    public static readonly BucketName Tracings = new("tracings", "application/json");
    public static readonly BucketName Networks = new("networks", "application/json");
}