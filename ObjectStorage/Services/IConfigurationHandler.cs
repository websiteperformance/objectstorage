namespace ObjectStorage.Services;

public interface IConfigurationHandler
{
    T GetValue<T>(string key);
}