using Akka.Actor;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging.Console;
using Microsoft.OpenApi.Models;
using Minio;
using Minio.AspNetCore.HealthChecks;
using ObjectStorage.Services;
using ObjectStorage.Services.Cache;
using ObjectStorage.Services.Inbound.StorageRemoveRequest;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Outbound.StorageResponse;
using ObjectStorage.Services.ServiceHooks;
using ObjectStorage.Services.Storage;
using ObjectStorage.Telemetry;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);
builder.Logging.AddSimpleConsole(options =>
{
    options.IncludeScopes = false;
    options.SingleLine = true;
    options.TimestampFormat = "[yyy-MM-dd HH:mm:ss.ffff]";
    options.ColorBehavior = LoggerColorBehavior.Enabled;
});
builder.Logging.AddFilter("*", LogLevel.Information);

builder.Services.AddControllers();

IConfigurationHandler configHandler = new ConfigurationHandler(builder.Configuration);
builder.Services.AddSingleton(configHandler);
builder.Services.AddMinio(configHandler);
builder.Services.AddRedis(configHandler);
builder.Services.AddSingleton<IStorageCacheAsync, StorageCacheRedis>();
builder.Services.AddSingleton<IMinioStorage, MinioStorage>();
builder.Services.AddSingleton<IStorageService, StorageService>();
builder.Services.AddScoped<IStorageProcessor, StorageProcessor>();
builder.Services.AddScoped<IRemoveStorageProcessor, RemoveStorageProcessor>();
builder.Services.AddSingleton<IMinioClientWrapper, MinioClientWrapper>();
builder.Services.AddScoped<IStoreResponseMessageProducer, StoreResponseMessageProducer>();
builder
    .Services
    .AddSilverback()
    .UseModel()
    .WithConnectionToMessageBroker(o => o.AddKafka())
    .AddEndpointsConfigurator<StorageRequestsEndpointConfigurator>()
    .AddEndpointsConfigurator<StorageResponseEndpointConfigurator>()
    .AddEndpointsConfigurator<StorageRemoveRequestEndpointConfigurator>()
    .AddScopedSubscriber<StorageRequestSubscriber>()
    .AddScopedSubscriber<StorageRemoveSubscriber>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSingleton(_ => ActorSystem.Create("ObjectStorageActorSystem"));

builder.Services
    .AddHealthChecks()
    .AddKafka(config => { config.BootstrapServers = configHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER"); },
        failureStatus: HealthStatus.Unhealthy, timeout: TimeSpan.FromSeconds(2), name: "KafkaHealthCheck")
    .AddMinio(d => d.GetRequiredService<MinioClient>())
    .AddRedis(d => d.GetRequiredService<IConnectionMultiplexer>());

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v2", new OpenApiInfo {Title = "ObjectStorage", Version = "v2"});
    c.EnableAnnotations();
});

var app = builder.Build();

MethodTimeLogger.Logger = app.Logger;
app.Lifetime.ApplicationStarted.Register(() => { app.Services.GetService<ActorSystem>(); });
app.Lifetime.ApplicationStopping.Register(() => { app.Services.GetService<ActorSystem>()?.Terminate().Wait(); });

if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c => { c.SerializeAsV2 = true; });
    app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v2/swagger.json", "ObjectStorage"); });
}

app.MapHealthChecks("/healthz", new HealthCheckOptions {ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse});
app.MapControllers();

app.Run();