using System.Reflection;

namespace ObjectStorage.Telemetry;

public static class MethodTimeLogger
{
    public static ILogger? Logger;

    public static void Log(MethodBase methodBase, long milliSeconds, string message)
    {
        Logger?.LogInformation("{Class}.{Method} :: {Elapsed}ms", methodBase.DeclaringType!.Name, methodBase.Name,
            milliSeconds);
    }
}