using System.Text.Json.Serialization;
using ObjectStorage.Services.Inbound.StorageRequest;

namespace ObjectStorage.Endpoints.StorageRequest;

public class StorageRequest
{
    [JsonPropertyName("id")] public Guid Id { get; set; }
    
    [JsonPropertyName("bucketName")] public PayloadTypeEnum? BucketName { get; set; }
}