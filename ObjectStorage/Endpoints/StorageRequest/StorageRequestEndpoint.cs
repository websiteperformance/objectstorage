using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using ObjectStorage.Services;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Storage;
using Swashbuckle.AspNetCore.Annotations;

namespace ObjectStorage.Endpoints.StorageRequest;

public class StorageRequestEndpoint(ILogger<StorageRequestEndpoint> logger, IMinioStorage minioStorage)
    : EndpointBaseAsync.WithRequest<StorageRequest>.WithActionResult
{
    [HttpPost("/api/storage/getItem")]
    [SwaggerOperation(
        Summary = "get item by id",
        Description = "try to retrieve an object by an id or give an 404 back",
        OperationId = "2eef2797-31db-4a04-ad4d-10dfedfa4cbe",
        Tags = new []{"Item"}
            )]
    [Produces(typeof(object))]
    [Consumes("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public override async Task<ActionResult> HandleAsync(StorageRequest request, CancellationToken cancellationToken = new())
    {
        logger.LogInformation("retrieve request for object id {OId} of bucket type {Tp}", request.Id.ToString(), request.BucketName);
        if (request.BucketName is null)
            return BadRequest();

        var btn = (PayloadTypeEnum) request.BucketName!;
        var bucket = BucketName.GetBucketNameByEnumType(btn);

        var resultOpt =
            await minioStorage.DownloadObjectAsync(bucket, request.Id.ToString());
        
        return resultOpt.Match<ActionResult>(
            Some: data =>
            {
                data.Position = 0;
                return new FileStreamResult(data, bucket.ContentType());
            },
            None: NotFound);
    }
}