using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using ObjectStorage.Services.Inbound.StorageRequest;

namespace ObjectStorage.Endpoints.DirectGetStoredObject;

public class DirectStorageRequest
{
    [FromRoute(Name = "id"), Required]
    public Guid Id { get; set; }

    [FromRoute(Name = "bucketName"), Required]
    public PayloadTypeEnum BucketName { get; set; }
}