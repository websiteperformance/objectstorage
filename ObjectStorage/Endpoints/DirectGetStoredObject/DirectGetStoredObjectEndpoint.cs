using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using ObjectStorage.Services;
using ObjectStorage.Services.Cache;
using ObjectStorage.Services.Storage;
using Swashbuckle.AspNetCore.Annotations;

namespace ObjectStorage.Endpoints.DirectGetStoredObject;

public class DirectGetStoredObjectEndpoint(ILogger<DirectGetStoredObjectEndpoint> logger, IMinioStorage minioStorage,
        IStorageCacheAsync storageCache)
    : EndpointBaseAsync.WithRequest<DirectStorageRequest>.WithActionResult
{
    [HttpGet("/api/storage/directGet/{bucketName}/{id}")]
    [SwaggerOperation(
        Summary = "download item",
        Description = "try to retrieve an object from objectstore and download to requester",
        OperationId = "a8ce7e14-a0eb-4c0d-8ae5-8be9ee77fa57",
        Tags = new[] {"Item"}
    )]
    [Produces(typeof(object))]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public override async Task<ActionResult> HandleAsync([FromRoute] DirectStorageRequest request,
        CancellationToken cancellationToken = new CancellationToken())
    {
        logger.LogInformation("retrieve request for object id {OId} of bucket type {Tp}", request.Id.ToString(),
            request.BucketName);

        var bucket = BucketName.GetBucketNameByEnumType(request.BucketName);

        return await storageCache
            .GetItemFromCacheAsync(request.Id)
            .MatchAsync(
                Some: async result =>
                {
                    logger.LogInformation("we get a hit from cache for key {Key}", request.Id);
                    var returnResult =
                        await Task.Run(() => new FileStreamResult(new MemoryStream(result), bucket.ContentType()),
                            cancellationToken);

                    return (ActionResult) returnResult;
                },
                None: async () =>
                {
                    logger.LogInformation("key {Key} not found in cache, lets get them from real object store",
                        request.Id);
                    var resultOpt = await minioStorage.DownloadObjectAsync(bucket, request.Id.ToString());
                    return await resultOpt.Match<Task<ActionResult>>(
                        Some: async data =>
                        {
                            logger.LogInformation("key {Key} found in object store, lets put data to cache",
                                request.Id);
                            data.Position = 0;
                            await storageCache.SetItemToCacheAsync(request.Id, data.ToArray());
                            data.Position = 0;
                            return new FileStreamResult(data, bucket.ContentType());
                        },
                        None: async () => await Task.FromResult(NotFound())
                    );
                });
    }
}