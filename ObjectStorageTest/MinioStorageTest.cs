using Microsoft.Extensions.Logging;
using Minio.DataModel.Args;
using Moq;
using ObjectStorage.Services;
using ObjectStorage.Services.Storage;

namespace ObjectStorageTest;

public class MinioStorageTest
{
    private readonly Mock<IMinioClientWrapper> _minioClientMock;
    private readonly IMinioStorage _minioStorage;

    public MinioStorageTest()
    {
        _minioClientMock = new Mock<IMinioClientWrapper>();
        Mock<ILoggerFactory> loggerFactoryMock = new();
        _minioStorage = new MinioStorage(loggerFactoryMock.Object, _minioClientMock.Object);
    }

    [Fact]
    public async Task CheckIfBucketExistsAsync_Calls_MinioClientBucketExistsAsync()
    {
        var bucket = BucketName.Timings;

        _minioClientMock.Setup(mc => mc.BucketExistsAsync(It.IsAny<BucketExistsArgs>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);

        var result = await _minioStorage.CheckIfBucketExistsAsync(bucket);

        Assert.True(result);
    }

    [Fact]
    public async Task DownloadObjectAsync_ReturnsMemoryStream_WhenDownloadSuccessful()
    {
        // Arrange
        var bucketName = BucketName.Timings;
        var objectName = "testObject";
        _minioClientMock.Setup(mc => mc.GetObjectAsync(It.IsAny<GetObjectArgs>(), It.IsAny<CancellationToken>()));

        // Act
        var result = await _minioStorage.DownloadObjectAsync(bucketName, objectName);

        // Assert
        Assert.True(result.IsSome);
    }

    [Fact]
    public async Task CreateBucketAsync_CreatesBucketSuccessfully()
    {
        // Arrange
        var bucketName = BucketName.Timings;

        // Act
        await _minioStorage.CreateBucketAsync(bucketName);

        // Assert
        _minioClientMock.Verify(mc => mc.MakeBucketAsync(It.IsAny<MakeBucketArgs>(), It.IsAny<CancellationToken>()),
            Times.Once);
    }

    [Fact]
    public async Task UploadDataAsync_UploadsDataSuccessfully()
    {
        // Arrange
        var bucketName = BucketName.Timings;
        var objectName = "testObject";
        var contentType = "application/octet-stream";
        var dataStream = new MemoryStream(new byte[] {1, 2, 3});

        // Act
        await _minioStorage.UploadDataAsync(dataStream, bucketName, objectName, contentType);

        // Assert
        _minioClientMock.Verify(mc => mc.PutObjectAsync(It.IsAny<PutObjectArgs>(), It.IsAny<CancellationToken>()),
            Times.Once);
    }
}