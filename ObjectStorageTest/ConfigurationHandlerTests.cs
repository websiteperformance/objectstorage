﻿using LanguageExt;
using Microsoft.Extensions.Configuration;
using Moq;
using ObjectStorage.Services;

namespace ObjectStorageTest;

public class ConfigurationHandlerTests
{
    [Fact]
    public void GetValue_ReturnsCorrectValue_WhenKeyExists()
    {
        // Arrange
        var mockConfigSection = new Mock<IConfigurationSection>();
        mockConfigSection.SetupGet(m => m.Value).Returns("TestValue");
        
        var mockConfiguration = new Mock<IConfiguration>();
        mockConfiguration.Setup(m => m.GetSection("TestKey")).Returns(mockConfigSection.Object);

        var configHandler = new ConfigurationHandler(mockConfiguration.Object);

        // Act
        string result = configHandler.GetValue<string>("TestKey");

        // Assert
        Assert.Equal("TestValue", result);
    }

    [Fact]
    public void GetValue_ThrowsException_WhenKeyDoesNotExist()
    {
        // Arrange
        string? returnValue = null;
        var mockConfigSection = new Mock<IConfigurationSection>();
        mockConfigSection.SetupGet(m => m.Value).Returns(returnValue);

        var mockConfiguration = new Mock<IConfiguration>();
        mockConfiguration.Setup(m => m.GetSection("TestKey")).Returns(mockConfigSection.Object);

        var configHandler = new ConfigurationHandler(mockConfiguration.Object);

        // Act & Assert
        Assert.Throws<ValueIsNullException>(() => configHandler.GetValue<string>("TestKey"));
    }

    [Fact]
    public void GetValue_IfItIsInteger()
    {
        var mockConfigSection = new Mock<IConfigurationSection>();
        mockConfigSection.SetupGet(m => m.Value).Returns("1");
        var mockConfiguration = new Mock<IConfiguration>();
        mockConfiguration.Setup(m => m.GetSection("TestInt")).Returns(mockConfigSection.Object);

        var configHandler = new ConfigurationHandler(mockConfiguration.Object);
        var returnValue = configHandler.GetValue<int>("TestInt");
        
        Assert.Equal(1, returnValue);

    }
}