﻿using Microsoft.Extensions.Logging;
using Moq;
using ObjectStorage.Services;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Storage;

namespace ObjectStorageTest;

public class StorageServiceTests
    {
        private readonly Mock<IMinioStorage> _minioStorageMock;
        private readonly IStorageService _storageService;

        public StorageServiceTests()
        {
            _minioStorageMock = new Mock<IMinioStorage>();
            Mock<ILogger<StorageService>> loggerMock = new();

            _storageService = new StorageService(loggerMock.Object, _minioStorageMock.Object);
        }

        [Fact]
        public async Task DoStorePayloadAsync_StoreSuccessful_ReturnsPayloadResponse()
        {
            var payloadDto = new PayloadDto
            {
                Id = Guid.NewGuid(),
                PayloadTypeEnum = PayloadTypeEnum.Timing,
                Data = new byte[] { 1, 2, 3 },
                ContentType = "application/octet-stream"
            };

            var bucket = BucketName.GetBucketNameByEnumType(payloadDto.PayloadTypeEnum); 
            _minioStorageMock.Setup(x => x.CheckIfBucketExistsAsync(bucket)).ReturnsAsync(true);

            var result = await _storageService.DoStorePayloadAsync(payloadDto, CancellationToken.None);

            Assert.NotNull(result);
            Assert.Equal(payloadDto.Id, result.Id);
            Assert.Equal("Ok", result.ResponseMessage);
            _minioStorageMock.Verify(m => m.UploadDataAsync(It.IsAny<Stream>(), bucket, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task DoStorePayloadAsync_StoreFails_ReturnsPayloadResponseWithError()
        {
            var payloadDto = new PayloadDto
            {
                Id = Guid.NewGuid(),
                PayloadTypeEnum = PayloadTypeEnum.Timing,
                Data = null,
                ContentType = null
            };

            var result = await _storageService.DoStorePayloadAsync(payloadDto, CancellationToken.None);

            Assert.NotNull(result);
            Assert.Equal(payloadDto.Id, result.Id);
            Assert.NotEqual("Ok", result.ResponseMessage);
            _minioStorageMock.Verify(m => m.UploadDataAsync(It.IsAny<Stream>(), It.IsAny<BucketName>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }
    }