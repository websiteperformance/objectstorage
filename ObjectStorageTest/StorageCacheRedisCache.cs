﻿using Microsoft.Extensions.Logging;
using Moq;
using ObjectStorage.Services.Cache;
using StackExchange.Redis;

namespace ObjectStorageTest;

public class StorageCacheRedisTests
{
    private readonly Mock<IDatabase> _mockDatabase;
    private readonly StorageCacheRedis _storageCache;

    public StorageCacheRedisTests()
    {
        _mockDatabase = new Mock<IDatabase>();
        Mock<IConnectionMultiplexer> mockConnectionMultiplexer = new();
        mockConnectionMultiplexer.Setup(m => m.GetDatabase(It.IsAny<int>(), It.IsAny<object>()))
            .Returns(_mockDatabase.Object);
        Mock<ILogger<StorageCacheRedis>> mockLogger = new();
        _storageCache = new StorageCacheRedis(mockLogger.Object, mockConnectionMultiplexer.Object);
    }

    [Fact]
    public async Task GetItemFromCacheAsync_ReturnsItem_IfExists()
    {
        var id = Guid.NewGuid();
        var data = new byte[] {1, 2, 3};
        _mockDatabase.Setup(d => d.StringGetAsync(id.ToString(), It.IsAny<CommandFlags>()))
            .ReturnsAsync(data);

        var result = await _storageCache.GetItemFromCacheAsync(id);

        Assert.True(result.IsSome);
        Assert.Equal(data, result.IfNone(() => null!));
    }

    [Fact]
    public async Task GetItemFromCacheAsync_ReturnsNone_IfDoesNotExist()
    {
        var id = Guid.NewGuid();
        _mockDatabase.Setup(d => d.StringGetAsync(id.ToString(), It.IsAny<CommandFlags>()))
            .ReturnsAsync(RedisValue.Null);

        var result = await _storageCache.GetItemFromCacheAsync(id);

        Assert.True(result.IsNone);
    }

    [Fact]
    public async Task SetItemToCacheAsync_SetsItem()
    {
        var id = Guid.NewGuid();
        var data = new byte[] {1, 2, 3};
        await _storageCache.SetItemToCacheAsync(id, data);

        _mockDatabase.Verify(
            d => d.StringSetAsync(id.ToString(), data, null, When.NotExists, CommandFlags.None),
            Times.Once);
    }

    [Fact]
    public async Task RemoveItemFromCacheAsync_RemovesItem()
    {
        var id = Guid.NewGuid();
        await _storageCache.RemoveItemFromCacheAsync(id);

        _mockDatabase.Verify(d => d.KeyDeleteAsync(id.ToString(), CommandFlags.None), Times.Once);
    }
}