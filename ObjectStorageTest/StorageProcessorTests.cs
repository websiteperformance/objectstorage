﻿using Microsoft.Extensions.Logging;
using Moq;
using ObjectStorage.Services;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Outbound.StorageResponse;
using ObjectStorage.Services.Storage;

namespace ObjectStorageTest;

public class StorageProcessorTests : Akka.TestKit.Xunit2.TestKit
{
    private readonly Mock<IStorageService> _storageServiceMock;
    private readonly Mock<IStoreResponseMessageProducer> _storeResponseMessageProducerMock;
    private readonly IStorageProcessor _storageProcessor;

    public StorageProcessorTests()
    {
        Mock<ILogger<StorageProcessor>> loggerMock = new();
        _storageServiceMock = new Mock<IStorageService>();
        _storeResponseMessageProducerMock = new Mock<IStoreResponseMessageProducer>();

        _storageProcessor = new StorageProcessor(
            loggerMock.Object,
            Sys,
            _storageServiceMock.Object,
            _storeResponseMessageProducerMock.Object
        );
    }

    [Fact]
    public async Task ProcessAsync_ProcessesValidMessages()
    {
        var id = Guid.NewGuid();
        
        var payloadDto = new PayloadDto
        {
            ContentType = "application/json",
            Data = new byte[] {1, 2, 3},
            Id = id,
            PayloadTypeEnum = PayloadTypeEnum.Timing
        };
        
        
        var payloadResponse = new PayloadResponse
        {
            BucketName = BucketName.Timings,
            Id = id,
            ResponseId = Guid.NewGuid(),
            ResponseMessage = "Popelliese"
        };
        
        _storageServiceMock.Setup(m => m.DoStorePayloadAsync(It.IsAny<PayloadDto>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(payloadResponse);

        var storageMessages = new List<PayloadDto> {payloadDto}.ToAsyncEnumerable();

        await _storageProcessor.ProcessAsync(storageMessages);

        _storageServiceMock.Verify(m => m.DoStorePayloadAsync(It.IsAny<PayloadDto>(), It.IsAny<CancellationToken>()),
            Times.Once);
        _storeResponseMessageProducerMock.Verify(m => m.PublishAsync(It.IsAny<PayloadResponse>()), Times.Once);
    }

}