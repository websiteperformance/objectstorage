﻿using Microsoft.Extensions.Logging;
using Moq;
using ObjectStorage.Services;
using ObjectStorage.Services.Outbound.StorageResponse;
using Silverback.Messaging.Publishing;

namespace ObjectStorageTest;

public class StoreResponseMessageProducerTests
{
    [Fact]
    public async Task PublishAsync_ReturnsTrue_WhenPublishSucceeds()
    {
        // Arrange
        var mockLogger = new Mock<ILogger<StoreResponseMessageProducer>>();
        var mockPublisher = new Mock<IPublisher>();
        var payloadResponse = new PayloadResponse
        {
            Id = Guid.NewGuid(),
            ResponseMessage = "message",
            ResponseId = Guid.NewGuid(),
            BucketName = BucketName.Timings
        };

        var returnValue = new List<PayloadResponseDto> {payloadResponse};

        mockPublisher.Setup(p => p.PublishAsync<PayloadResponseDto>(It.IsAny<PayloadResponseDto>()))
            .Returns(Task.FromResult<IReadOnlyCollection<PayloadResponseDto>>(returnValue));

        var producer = new StoreResponseMessageProducer(mockLogger.Object, mockPublisher.Object);

        // Act
        var result = await producer.PublishAsync(payloadResponse);

        mockPublisher.Verify(p => p.PublishAsync<PayloadResponseDto>(It.IsAny<PayloadResponseDto>()), Times.Once);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public async Task PublishAsync_ReturnsFalse_WhenPublishFails()
    {
        // Arrange
        var mockLogger = new Mock<ILogger<StoreResponseMessageProducer>>();
        var mockPublisher = new Mock<IPublisher>();
        var payloadResponse = new PayloadResponse
        {
            Id = Guid.NewGuid(),
            ResponseMessage = "message",
            ResponseId = Guid.NewGuid(),
            BucketName = BucketName.Timings
        };

        var returnValue = new List<PayloadResponseDto> {payloadResponse};
        mockPublisher.Setup(p => p.PublishAsync<PayloadResponseDto>(It.IsAny<PayloadResponseDto>()))
            .Throws(new Exception("an error occured"));    

        var producer = new StoreResponseMessageProducer(mockLogger.Object, mockPublisher.Object);

        // Act
        var result = await producer.PublishAsync(payloadResponse);

        mockPublisher.Verify(p => p.PublishAsync<PayloadResponseDto>(It.IsAny<PayloadResponseDto>()), Times.Once);
        // Assert
        Assert.False(result);
    }
}