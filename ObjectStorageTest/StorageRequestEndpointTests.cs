using LanguageExt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using ObjectStorage.Endpoints.StorageRequest;
using ObjectStorage.Services;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Storage;

namespace ObjectStorageTest;

public class StorageRequestEndpointTests
{
    private readonly Mock<ILogger<StorageRequestEndpoint>> _mockLogger = new();
    private readonly Mock<IMinioStorage> _mockMinioStorage = new();

    [Fact]
    public async Task HandleAsync_ReturnsBadRequest_WhenBucketNameIsNull()
    {
        var request = new StorageRequest { Id = Guid.NewGuid(), BucketName = null };
        var endpoint = new StorageRequestEndpoint(_mockLogger.Object, _mockMinioStorage.Object);

        var result = await endpoint.HandleAsync(request);

        Assert.IsType<BadRequestResult>(result);
    }

    [Fact]
    public async Task HandleAsync_ReturnsNotFound_WhenObjectNotFound()
    {
        var request = new StorageRequest { Id = Guid.NewGuid(), BucketName = PayloadTypeEnum.Timing };
        _mockMinioStorage.Setup(m => m.DownloadObjectAsync(It.IsAny<BucketName>(), It.IsAny<string>()))
            .ReturnsAsync(Option<MemoryStream>.None);

        var endpoint = new StorageRequestEndpoint(_mockLogger.Object, _mockMinioStorage.Object);

        var result = await endpoint.HandleAsync(request);

        Assert.IsType<NotFoundResult>(result);
    }
}