﻿using LanguageExt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using ObjectStorage.Endpoints.DirectGetStoredObject;
using ObjectStorage.Services;
using ObjectStorage.Services.Cache;
using ObjectStorage.Services.Inbound.StorageRequest;
using ObjectStorage.Services.Storage;

namespace ObjectStorageTest;

public class DirectGetStoredObjectEndpointTests
{
    // [Fact]
    // public async Task HandleAsync_ReturnsFileStreamResult_WhenItemIsInCache()
    // {
    //     // Arrange
    //     var mockLogger = new Mock<ILogger<DirectGetStoredObjectEndpoint>>();
    //     var mockMinioStorage = new Mock<IMinioStorage>();
    //     var mockStorageCache = new Mock<IStorageCache>();
    //     byte[] data = {1, 2, 3, 4, 5};
    //
    //     mockStorageCache.Setup(s => s.GetItemFromCache(It.IsAny<Guid>()))
    //         .Returns(Option<byte[]>.Some(data));
    //
    //     var endpoint = new DirectGetStoredObjectEndpoint(
    //         mockLogger.Object, mockMinioStorage.Object, mockStorageCache.Object);
    //
    //     // Act
    //     var result = await endpoint.HandleAsync(new DirectStorageRequest
    //     {
    //         Id = Guid.NewGuid(),
    //         BucketName = PayloadTypeEnum.Screenshot
    //     });
    //     
    //     mockStorageCache.Verify(d => d.GetItemFromCache(It.IsAny<Guid>()), Times.Once);
    //
    //     // Assert
    //     Assert.IsType<FileStreamResult>(result);
    // }
    //
    // [Fact]
    // public async Task HandleAsync_ReturnsNotFound_WhenItemIsNotInCacheNorStorage()
    // {
    //     // Arrange
    //     var mockLogger = new Mock<ILogger<DirectGetStoredObjectEndpoint>>();
    //     var mockMinioStorage = new Mock<IMinioStorage>();
    //     var mockStorageCache = new Mock<IStorageCache>();
    //
    //     mockStorageCache.Setup(s => s.GetItemFromCache(It.IsAny<Guid>()))
    //         .Returns(Option<byte[]>.None);
    //
    //     mockMinioStorage.Setup(m => m.DownloadObjectAsync(It.IsAny<BucketName>(), It.IsAny<string>()))
    //         .ReturnsAsync(Option<MemoryStream>.None);
    //
    //     var endpoint = new DirectGetStoredObjectEndpoint(
    //         mockLogger.Object, mockMinioStorage.Object, mockStorageCache.Object);
    //
    //     // Act
    //     var result = await endpoint.HandleAsync(new DirectStorageRequest
    //     {
    //         Id = Guid.NewGuid(),
    //         BucketName = PayloadTypeEnum.Screenshot
    //     });
    //
    //     // Assert
    //     Assert.IsType<NotFoundResult>(result);
    // }
}