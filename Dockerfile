﻿FROM harbor.gretzki.ddns.net/ms-cache/dotnet/aspnet:latest
WORKDIR /app
COPY build_output/ ./
EXPOSE 5777
ENV ASPNETCORE_URLS=http://+:5777
ENTRYPOINT ["dotnet", "ObjectStorage.dll"]